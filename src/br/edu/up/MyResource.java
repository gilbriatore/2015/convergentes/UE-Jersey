package br.edu.up;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/resource")
public class MyResource {
    @GET
    @Produces({"application/xml", MediaType.APPLICATION_JSON})
    public MyBean getMyBean() {
        return new MyBean("Hello World!", 42);
    }
 
    @POST
    @Consumes("application/xml")
    public String postMyBean(MyBean myBean) {
        return myBean.anyString;
    }
}